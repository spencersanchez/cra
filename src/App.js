// @flow
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';
import Doodad from './containers/Doodad/';

const styles = {
	container: {
		margin: '0 auto',
		textAlign: 'left',
		width: '400px'
	}
}

class App extends Component {
	render() {
		return (
			<div className="App">
				<div className="App-header">
					<img src={logo} className="App-logo" alt="logo" />
					<h2>Creat React App Demo</h2>
				</div>
				<p className="App-intro">Complete with HMR, React Storybook, and Flow.</p>

				<Doodad styles={styles} />
			</div>
		);
	}
}

export default App;
