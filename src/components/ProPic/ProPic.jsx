import React, { Component, PropTypes } from 'react'

class ProPic extends Component {

	shouldComponentUpdate() {
		return false
	}

	render() {
		const innerStyles = Object.assign({}, {
			display: 'inline',
			height: '100%',
			margin: '0 auto',
			width: 'auto'
		 }, this.props.styles.inner)

		const outerStyles = Object.assign({}, {
			borderRadius: '50%',
			height: '100px',
			overflow: 'hidden',
			position: 'relative',
			width: '100px'
		}, this.props.styles.outer)

		return (
			<div style={outerStyles}>
				<img src={this.props.src} alt={this.props.alt} style={innerStyles} />
			</div>
		)
	}
}

ProPic.propTypes = {
	alt: PropTypes.string,
	bdrRadius: PropTypes.string,
	src: PropTypes.string,
	styles: PropTypes.shape({
		inner: PropTypes.object,
		outer: PropTypes.object
	})
}

ProPic.defaultProps = {
	alt: 'placeholder',
	src: 'http://dummyimage.com/100x100/4d494d/686a82.gif&text=placeholder',
	styles: {}
}

export default ProPic
