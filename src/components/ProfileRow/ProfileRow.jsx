import React, { Component, PropTypes } from 'react'
import Text from '../Text'
import ProPic from '../ProPic'

class ProfileRow extends Component {

	render() {
		return (
			<div style={this.props.styles.row}>
				<ProPic src={this.props.src} styles={this.props.styles.proPic} />
				<Text txt={this.props.nme} styles={this.props.styles.nme} />
			</div>
		)
	}
}

ProfileRow.propTypes = {
	nme: PropTypes.string,
	src: PropTypes.string,
	styles: PropTypes.object
}

ProfileRow.defaultProps = {
	styles: {}
}

export default ProfileRow
