import React, { Component, PropTypes } from 'react'

class Text extends Component {

	shouldComponentUpdate() {
		return false
	}

	render() {
		return (
			<span style={this.props.styles}>{this.props.txt}</span>
		)
	}
}

Text.propTypes = {
	styles: PropTypes.object,
	txt: PropTypes.string
}

Text.defaultProps = {
	txt: 'Some inspirational text...'
}

export default Text
