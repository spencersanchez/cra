import React, { Component, PropTypes } from 'react'
import BzLogo from '../../components/BzLogo'
import Text from '../../components/Text'
import ProfileRow from '../../components/ProfileRow'

// profile pics
import guilluame from '../../img/guilluame.png'
import marc from '../../img/marc.png'
import peter from '../../img/peter.png'
import ryan from '../../img/ryan.jpg'
import samantha from '../../img/samantha.png'
import spencer from '../../img/spencer.png'
import steve from '../../img/steve.png'

const styles = {
	container: {
		backgroundColor: '#163353',
		padding: '20px',
		width: '400px'
	},
	logo: {
		display: 'block',
		height: 'auto',
		width: '100%'
	},
	profileRow: {
		nme: {
			color: '#fff',
			display: 'inline-block',
			paddingLeft: '2em',
			verticalAlign: 'middle'
		},
		proPic: {
			outer: {
				border: '1px solid #fff',
				boxShadow: '0px 0px 20px 0px rgba(255,255,255,0.75)',
				display: 'inline-block',
				height: '50px',
				verticalAlign: 'middle',
				width: '50px'
			}
		},
		row: {
			marginTop: '2em'
		}
	},
	subheader: {
		color: 'white',
		display: 'block',
		fontStyle: 'italic',
		margin: '1em 0'
	}
}

class Doodad extends Component {
	render() {
		const containerStyles = Object.assign({}, styles.container, this.props.styles.container)
		const logoStyles = Object.assign({}, styles.logo, this.props.styles.logo)
		const subheaderStyles = Object.assign({}, styles.subheader, this.props.styles.subheader)
		const profileRowStyles = Object.assign({}, styles.profileRow, this.props.styles.profileRow)
		return (
			<div style={containerStyles}>
				<BzLogo accentColor='#3F91EC' baseFill='#fff' styles={logoStyles} />
				<Text txt='...and other properties' styles={subheaderStyles} />
				<Text txt='B2B Front End Engineering' styles={subheaderStyles} />
				<ProfileRow src={marc} styles={profileRowStyles} nme='Marc Arbesman' />
				<ProfileRow src={samantha} styles={profileRowStyles} nme='Samantha Chan' />
				<ProfileRow src={ryan} styles={profileRowStyles} nme='Ryan Doherty' />
				<ProfileRow src={guilluame} styles={profileRowStyles} nme='Guilluame Hugot' />
				<ProfileRow src={peter} styles={profileRowStyles} nme='Peter Nguyen' />
				<ProfileRow src={spencer} styles={profileRowStyles} nme='Spencer Sanchez' />
				<ProfileRow src={steve} styles={profileRowStyles} nme='Steve Watkins' />
			</div>
		)
	}
}

Doodad.propTypes = {
	styles: PropTypes.object
}

Doodad.defaultProps = {
	styles: {}
}

export default Doodad
