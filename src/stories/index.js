import React from 'react'
// import { storiesOf, action, linkTo } from '@kadira/storybook'
import { storiesOf } from '@kadira/storybook'
// import Button from './Button'
import BzLogo from '../components/BzLogo'
import Text from '../components/Text'
import ProPic from '../components/ProPic'
import ProfileRow from '../components/ProfileRow'
import Doodad from '../containers/Doodad'

// storiesOf('Welcome', module)
//   .add('to Storybook', () => (
//     <Welcome showApp={linkTo('Button')}/>
//   ))

// storiesOf('Button', module)
// 	.add('with text', () => (
// 		<Button onClick={action('clicked')}>Hello Button</Button>
// 	))
// 	.add('with some emoji', () => (
// 		<Button onClick={action('clicked')}>😀 😎 👍 💯</Button>
// 	))

const styles = {
	header: {
		color: 'black',
		fontSize: '2rem',
		fontWeight: '800'
	},
	logoDimensions: {
		height: 'auto',
		width: '400px'
	},
	proPicDimensions: {
		outer: {
			height: '50px',
			width: '50px'
		}
	},
	subheader: {
		color: 'darkgray',
		fontStyle: 'italic'
	}
}

storiesOf('doodad.BzLogo', module)
	.add('red accent', () => (
		<BzLogo accentColor='#f00' baseFill='#ccc' />
	))
	.add('blue accent', () => (
		<BzLogo accentColor='#00f'/>
	))
	.addWithInfo(
		'full example',
		`
			This is the BuyerZone logo with changable base fill and accent colors.
		`,
		() => (
			<BzLogo accentColor='#F49445' baseFill='#163353' styles={styles.logoDimensions} />
		),
		{ inline: true }
	)

storiesOf('doodad.Text', module)
	.addWithInfo(
		'Header',
		`
			Great for commuinincating main intent
		`,
		() => (
			<Text txt='Testing the Text component as a header' styles={styles.header} />
		),
		{ inline: true }
	)
	.addWithInfo(
		'Subheader',
		`
			Useful for underneath a logo or header
		`,
		() => (
			<Text txt='Testing the Text component as a subheader' styles={styles.subheader} />
		),
		{ inline: true }
	)

const sx = ['men','women'][Math.floor(Math.random() * 2)]
const idx = Math.floor(Math.random() * 99) + 1
const rando = `https://randomuser.me/api/portraits/${sx}/${idx}.jpg`
storiesOf('doodad.ProPic', module)
	.addWithInfo(
		'default',
		`
			Circularly masked profile image, default state
		`,
		() => (
			<ProPic />
		),
		{ inline: true }
	)
	.addWithInfo(
		'Slightly rounded corners, random person',
		`
			Circularly masked actual profile pic, slightly rounded
		`,
		() => (
			<ProPic src={rando} />
		),
		{ inline: true }
	)
	.addWithInfo(
		'Fully rounded propic',
		`
			Circularly masked actual profile pic in a circle
		`,
		() => (
			<ProPic src={rando} styles={styles.proPicDimensions} />
		),
		{ inline: true }
	)


storiesOf('doodad.ProfileRow', module)
	.addWithInfo(
		'default',
		`
			Profile row: pic plus name, default
		`,
		() => (
			<ProfileRow />
		),
		{ inline: true }
	)
	.addWithInfo(
		'Random person',
		`
			Profile row: pic plus name, random person
		`,
		() => (
			<ProfileRow src={rando} nme={sx === 'men' ? 'John Doe' : 'Jane Doe'} />
		),
		{ inline: true }
	)


storiesOf('doodad.Doodad', module)
	.addWithInfo(
		'Supah Team',
		`
			Here's a widgit-ish Doodad for the B2B Front End Engineering team.
		`,
		() => (
			<Doodad />
		),
		{ propTables: false, inline: true }
	)
